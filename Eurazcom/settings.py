import os
from datetime import timedelta
from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
# Ajouter le template
TEMPLATES_DIRS = os.path.join(BASE_DIR,'templates') 

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-sl8l=axw0^xc-9%28v4k=jf9-n(0yq7i1!#besa@#ub5zbh6@h'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Email settings
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_FROM = 'emmanya30@gmail.com'
EMAIL_HOST_USER = 'emmanya30@gmail.com'
EMAIL_HOST_PASSWORD = 'psee uhxi jzkh musl'
EMAIL_PORT = 587
EMAIL_USE_TLS = True


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'axes',
    # custome apps
    'Authentication',
    'Usermanager',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 
    'axes.middleware.AxesMiddleware',
]

ROOT_URLCONF = 'Eurazcom.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATES_DIRS],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'Eurazcom.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'axes.backends.AxesBackend',
]
AUTH_USER_MODEL ='Authentication.CustomUser'

# django-axes configuration
AXES_LOGIN_FAILURE_LIMIT = 30  # Limite le nombre de tentatives de connexion infructueuses
AXES_LOCK_OUT_BY_COMBINATION_USER_AND_IP  = True
AXES_USERNAME_FORM_FIELD='email'
AXES_LOCK_OUT_AT_FAILURE = True  # Bloque automatiquement les adresses IP après dépasser la limite de tentatives
AXES_USE_USER_AGENT = True  # Prend en compte le User-Agent lors du blocage des adresses IP
AXES_COOLOFF_TIME = timedelta(minutes=1)  # Durée du blocage d'une adresse IP
AXES_LOCKOUT_TEMPLATE = 'authentication/exceptions/account_locked.html'

# Vous pouvez également personnaliser d'autres paramètres de configuration selon vos besoins

# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'

# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
LOGIN_URL = '/user/signin/'
# LOGIN_REDIRECT_URL = '/welcome/'

 
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]

# STATICFILES_DIRS = ['static']

MEDIA_ROOT=os.path.join(BASE_DIR,'media')

MEDIA_URL='/media/'