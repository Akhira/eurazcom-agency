from django.contrib import admin
from django.urls import path,include

from django.conf import settings
from django.conf.urls.static import static
# from axes.decorators import axes_dispatch

urlpatterns = [
	path('user/', include('Authentication.urls',namespace="Auth")),

	path('user/auth/', include('Usermanager.urls',namespace="Usermanager")),

    path('admin/', admin.site.urls),
    # path('admin/axes/', axes_dispatch),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
