import json
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes, force_str
from django.core.mail import EmailMessage

# owns imports
from .token import account_activate_token

class Utilities():

	# send user mail
	def send_user_mail(self,request, user, to_email, mail_subject, template_mail, otp_code=" "):
	    message = render_to_string(template_mail,{
	        'user':user,
	        'otp_code' : otp_code,
	        'domain':get_current_site(request).domain,
	        'uid':urlsafe_base64_encode(force_bytes(user.pk)),
	        'token':account_activate_token.make_token(user),
	        'protocol':'https' if request.is_secure() else 'http'
	    })
	    email = EmailMessage(mail_subject, message, to=[to_email])
	    email.content_subtype = 'html'
	    email.send()

	# redirect user dynamicaly
	def redirect_with_user_kwargs(self,route,project_number=None):
		user = request.user
		user_name = user.name.split(" ")
		name = user_name[0]
		id_user = urlsafe_base64_encode(json.dumps(user.email+user.name).encode('utf-8'))
		return reverse(route, kwargs={'user_name':name,'user_token':id_user,'project_number':project_number})

	# Methode qui verifie si tous les champs obligatoires d'un 
	# model precis possedent au moins une valeur differente de None.
	def check_required_fields(self,model):
		required_fields = [field.name for field in model._meta.fields if field.blank is False and field.null is False]
		objects_with_missing_fields=model.objects.filter(**{f'{field}__isnull':True for field in required_fields})
		model_name = None
		if objects_with_missing_fields:
			for obj in objects_with_missing_fields:
				model_name = obj._meta.verbose_name
				return model_name
		else:
			return model_name