from functools import wraps
from django.shortcuts import redirect
from django.urls import reverse 

def guest_only(view_func):
    @wraps(view_func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
        	pass
            # return redirect(reverse('welcome'))
        return view_func(request, *args, **kwargs)
    return wrapper