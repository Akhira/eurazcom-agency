from django import forms
import re
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.forms.utils import ErrorList

# import owns models
User=get_user_model()

class ParagraphErrorList(ErrorList):
    def __str__(self):
        return self.as_divs()

    def as_divs(self):
        if not self:return ''
        return '<div class="errorlist text-danger">%s</div>'%''.join(['<p class="small error">%s</p>'%e for e in self])

# login account  form
class LoginUserForms(forms.Form):
    email = forms.CharField(widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Votre adresse email'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Mot de passe'}))
 
# 2FA form
class User2FAForms(forms.Form):
	otp_secret = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','style':'box-shadow:none;','placeholder':'Entrer votre code de securite'}))
	 
	def clean_security_code(self):
		otp_secret = self.cleaned_data.get('otp_secret')
		if otp_secret.isdigit():
			raise forms.ValidationError(_('Format de code incorrect'))
		return otp_secret
		
			
# create account form
class CustomUserForms(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'votre mot de passe'}))
	password_confirm = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Confirmez votre mot de passe'}))

	CHOICE_STATUT = (
		('Particular','Un particular'),
		('Comapny','Une entreprise'),
	)
	status = forms.ChoiceField(
		label='Quel est votre status ? Etes-vous ?',
		choices=CHOICE_STATUT, 
		initial='Particular',
		widget=forms.RadioSelect(attrs={'class':'form-check-input'}))

	class Meta:
		model=User
		fields = ['name','email','phone','status','password']

	def clean_password(self):
		password = self.cleaned_data.get('password')
		if len(password) < 8:
			raise forms.ValidationError("Le mot de passe doit comporter au moins 8 caractères.")

		if not re.search(r'[A-Z]', password):
			raise forms.ValidationError("Le mot de passe doit contenir au moins une lettre majuscule.")

		if not re.search(r'\d', password):
			raise forms.ValidationError("Le mot de passe doit contenir au moins un chiffre.")

		if not re.search(r'[!@#$%^&*(),.?":{}|<>]', password):
			raise forms.ValidationError("Le mot de passe doit contenir au moins un caractère spécial.")

		return password

	def clean_password_confirm(self):
		password = self.cleaned_data.get('password')
		password_confirm = self.cleaned_data.get('password_confirm')

		if password and password_confirm and password != password_confirm:
			raise forms.ValidationError("Les mots de passe ne correspondent pas.")

		return password_confirm

	def __init__(self, *args, **kwargs):
	    super(CustomUserForms, self).__init__(*args, **kwargs)
	    for field_name, field in self.fields.items():
	        widget_class = field.widget.attrs.get('class', '').split()
	        if 'form-check-input' not in widget_class:
	            if field_name == 'password':
	                field.widget.attrs['type']='password'
	                field.widget.attrs['data-parsley-pattern'] = r'^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*(),.?":{}|<>])[A-Za-z\d!@#$%^&*(),.?":{}|<>]{8,}$'
	                field.widget.attrs['data-parsley-pattern-message'] = 'Au moins 8 caractères, une lettre majuscule, un chiffre et un caractère spécial.'
	            
	            if field_name == 'password_confirm':
	            	field.widget.attrs['data-parsley-equalto'] = '#id_password'
	            	field.widget.attrs['data-parsley-equalto-message']= 'Les mots de passe ne correspondent pas.'
				      					
	            field.widget.attrs['class'] = field.widget.attrs.get('class', '') + ' form-control'
	            field.widget.attrs['id'] = 'id_' + f'{field_name}'
	            field.widget.attrs['data-parsley-required'] = 'true'
	            field.widget.attrs['style'] = field.widget.attrs.get('style', '') + 'box-shadow:none;'
	            if field_name != 'password' and field_name !='password_confirm':
		            field.widget.attrs['placeholder'] = field.widget.attrs.get('placeholder', '') + f'Votre {field_name}'
 


class GeneratedNewLinkForm(forms.Form):
	email = forms.CharField(widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Votre adresse email'}))
	def clean_password_confirm(self):
		password = self.cleaned_data.get('password')
		password_confirm = self.cleaned_data.get('password_confirm')
		if password and password_confirm and password != password_confirm:
			raise forms.ValidationError("Les mots de passe ne correspondent pas.")

		return password_confirm

class UserForgotPasswordForm(forms.Form):
	email = forms.CharField(widget=forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'Votre adresse email'}))

	def clean_email(self):
	    email = self.cleaned_data.get('email')
	    pattern = r'^[\w\.-]+@[\w\.-]+\.\w+$'
	    if not re.match(pattern, email):
	    	raise forms.ValidationError("Cette adresse e-mail n'est pas valide.")

	    user_exists = User.objects.filter(email=email).exists()
	    if not user_exists:
	        raise forms.ValidationError("Cette adresse e-mail n'est associée à aucun compte.")
	    return email



class ResetPasswordForm(forms.Form):
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'New password'}))
	password_confirm = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Confirm new password'}))

	def clean_password(self):
		password = self.cleaned_data.get('password')
		if len(password) < 8:
			raise forms.ValidationError("Le mot de passe doit comporter au moins 8 caractères.")

		if not re.search(r'[A-Z]', password):
			raise forms.ValidationError("Le mot de passe doit contenir au moins une lettre majuscule.")

		if not re.search(r'\d', password):
			raise forms.ValidationError("Le mot de passe doit contenir au moins un chiffre.")

		if not re.search(r'[!@#$%^&*(),.?":{}|<>]', password):
			raise forms.ValidationError("Le mot de passe doit contenir au moins un caractère spécial.")

		return password
		
	def clean_password_confirm(self):
		password = self.cleaned_data.get('password')
		password_confirm = self.cleaned_data.get('password_confirm')
		if password and password_confirm and password != password_confirm:
			raise forms.ValidationError("Les mots de passe ne correspondent pas.")

		return password_confirm