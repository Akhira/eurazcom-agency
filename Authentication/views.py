from django.http import JsonResponse
from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model,authenticate, login, logout
from django.contrib.auth.backends import ModelBackend
from django.core.exceptions import ValidationError
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.views.generic import FormView,TemplateView
from django.db import OperationalError
from django.utils.http import urlsafe_base64_decode,urlsafe_base64_encode
from django.utils.encoding import force_str
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from axes.decorators import axes_dispatch
from django.contrib.auth.hashers import make_password
import pyotp, json
from datetime import datetime
from django.urls import reverse
# from django.contrib.auth.tokens import default_token_generator
 
from django.http import HttpResponse
 

from .forms import (
	ParagraphErrorList,
	CustomUserForms,
	LoginUserForms,
	User2FAForms,
	GeneratedNewLinkForm,
	ResetPasswordForm,
	UserForgotPasswordForm
)

from .helpers import ActivationAccount
from .token import account_activate_token
from .decorators import guest_only

User=get_user_model()

		
###########################################################
##################### CREATE ACCOUNT   ####################
##########################################################
@method_decorator(guest_only,name='dispatch')
class UserCreationAccountView(ActivationAccount, FormView):
	template_name = 'authentication/pages/register.html'
	page_title = 'Devenez client Eurazcom'
	mail_subject="Account Activation"
	template_mail="authentication/activation/template_activate_account.html"

	# show register page template
	def get(self,request):
		form = CustomUserForms(initial={'status':'Particular'})
		context={'form':form, 'page_title':self.page_title}
		return render(request,self.template_name,context)

	def post(self,request):
		form = CustomUserForms(request.POST,initial={'status':'Particular'}, error_class=ParagraphErrorList)
		context={'form':form, 'page_title':self.page_title}
		try:
			if form.is_valid():	            
				user = form.save(commit=False)
				user.country = 'Cameroun'
				user.city='Douala'
				user.state='LT'
				user.password=make_password(form.cleaned_data.get('password'))
				user.save()
				try:
					self.send_user_mail(request,user,user.email,self.mail_subject,self.template_mail)
				except Exception as e:
					messages.info(request,"Echec de l'envoi du mail, verifier la connexion")
				else:
					request.session['create_user_email'] = user.email
					return self.account_create_success(request)
			if not form.is_valid():
				context['errors'] = form.errors.items()
			return render(request,self.template_name,context)
		except OperationalError:
			return render(request,'exceptions/errors_access_to_database.html')
 
	# activate account logic
	@classmethod
	def activate_account(cls,request, uidb64, token):
	    try:
	        uid = force_str(urlsafe_base64_decode(uidb64))
	        user = User.objects.get(pk=uid)
	    except (TypeError, ValueError, OverflowError,User.DoesNotExist):
	        user = None
 		 
	    if user is not None and user.is_active:
    		return cls.account_already_activate(request, uidb64, token)

	    if user is not None and account_activate_token.check_token(user,token):
	    	
	    	if not user.is_active:
		        user.is_active = True
		        user.save()
		        return cls.account_activate_success(request, uidb64, token)
	    else:
	        return cls.account_activate_failed(request,uidb64,token)

	# Genrate eand send new activation link system
	@classmethod
	def generate_new_activation_link(cls,request):

		if 'create_user_email' in request.session:
			user_email = request.session['create_user_email']
			user = User.objects.get(email=user_email)
			if user is not None:
				try:
					instance = cls()
					instance.mail_subject = cls.mail_subject
					instance.template_mail = cls.template_mail
					instance.send_user_mail(request, user, user.email,instance.mail_subject,instance.template_mail)
				except Exception as e:
					messages.error(request,"Une erreur c'est produite lorsde l'envoi du mail ")
					return cls.account_create_success(request)
				else:
					messages.success(request,"Mail envoye avec succes")
					return cls.account_create_success(request)
		else:
			form = GeneratedNewLinkForm(request.POST, error_class=ParagraphErrorList)
			if request.method == 'POST':
				context = {'form':forn}
				if form.is_valid():
					form_mail = form.cleaned_data.get('email')
					user = User.objects.get(email=form_mail)
					if user is not None:
						try:
							instance.mail_subject = cls.mail_subject
							instance.template_mail = cls.template_mail
							instance.send_user_mail(request, user, user.email,instance.mail_subject,instance.template_mail)
						except Exception as e:
							messages.error(request,"Une erreur c'est produite lorsde l'envoi du mail ")
							return cls.account_create_success(request)
						else:
							messages.success(request,"Mail envoye avec succes")
							return cls.account_create_success(request)
				else:
					context['errors'] = form.errors.items()
			else:
				form = GeneratedNewLinkForm()
			return render(request,'authentication/pages/account_create_success.html',context)
		

	# Get successful account created view
	@classmethod
	def account_create_success(cls, request):
		return render(request, 'authentication/pages/account_create_success.html',{'page_title': 'Compte créé avec succès'})

	
	# Get filled account created view
	@classmethod
	def account_create_failed(cls,request):
		return render(request,'authentication/pages/account_create_failed.html',{'page_title': 'Echec de creation de compte'})

	# Get successful account activated view
	@classmethod
	def account_activate_success(cls,request, uidb64, token):
		if 'create_user_email' in request.session:
			del request.session['create_user_email']
		return render(request,'authentication/activation/activate_success.html',{'page_title': 'Activaion reussie'})

	# Get failled account activated view
	@classmethod
	def account_activate_failed(cls,request, uidb64, token):
		return render(request,'authentication/activation/activate_failed.html',{'page_title': 'Echec activaion compte'})
    
    # Get account already activated view
	@classmethod
	def account_already_activate(cls,request, uidb64, token):
		return render(request,'authentication/activation/account_already_activate.html',{'page_title':'Account already activate'})
		# return render(request,'authentication/activation/account_already_activate.html')
	

###########################################################
##################### CONNEXION SYSTEM ####################
##########################################################
@method_decorator(guest_only,name='dispatch')
class UserConnexionAccountView(ActivationAccount,FormView):
	template_name='authentication/pages/login.html'
	page_title='Connexion a votre espace client'

	def get(self,request):
		form = LoginUserForms()
		context={'form':form, 'page_title':self.page_title}
		return render(request,self.template_name,context)

	def post(self, request):
		form = LoginUserForms(request.POST,error_class=ParagraphErrorList)
		context = {'form': form, 'page_title': self.page_title}
		try:
			if form.is_valid():
				email = form.cleaned_data.get('email')
				password = form.cleaned_data.get('password')
				user = authenticate(request, email=email, password=password)
				if user is None or user.is_closed:
					messages.error(request, "Adresse email ou mot de passe est incorrecte")

				if user is not None and not user.is_active:
					messages.info(request, 'Compte non active')
					return self.account_not_actived(request)

				if user is not None and user.is_suspended:
					messages.warning(request, 'Votre compte a ete suspendu')
					return self.account_suspended(request)

				if user is not None and user.is_active:
					try:
						self.send_otp(request,user,user.email)
					except Exception as e:
						messages.error(request,"!!! Des soucis lors de l'envoi du code, reessayez plus tard")
					else:
						request.session['user_email'] = email
						return redirect('auth:2fa_account_verify')

			if not form.is_valid():
				context['errors'] = form.errors.items()

		except OperationalError:
			return render(request, 'exceptions/errors_access_to_database.html')

		return render(request, self.template_name, context)

	# Account suspended view
	@classmethod
	def account_suspended(cls,request):
		return render(request,'exceptions/account_suspended.html')

	@classmethod
	def account_not_actived(cls,request):
		return render(request,'exceptions/account_not_actived.html')


###########################################################
########################### 2FA SYSTEM ####################
##########################################################
# @method_decorator(csrf_exempt, name='dispatch')
@method_decorator(guest_only,name='dispatch')
class User2FAView(ActivationAccount, FormView):
    template_name = 'authentication/pages/2fa.html'
    page_title = 'Vérifier votre identité'

    # Vue 2FA
    def get(self, request):
        form = User2FAForms()
        context = {'form': form, 'page_title': self.page_title}
        return render(request, self.template_name, context)

    # Vérification du code
    def post(self, request):
        form = User2FAForms(request.POST, error_class=ParagraphErrorList)
        context = {'form': form, 'page_title': self.page_title}
        if form.is_valid():
        	otp = form.cleaned_data.get('otp_secret')
        	user_email = None
        	if 'user_email' in request.session:
	        	user_email = request.session['user_email']
	        else:
	        	return redirect("auth:sign_in")
        	otp_secret_key = request.session['otp_secret_key']
        	otp_valid_date = request.session['otp_valid_date']
        	if otp_valid_date and otp_secret_key is not None:
        		valid_date = datetime.strptime(otp_valid_date, '%Y-%m-%d %H:%M:%S.%f')
        		if valid_date > datetime.now():
        			totp = pyotp.TOTP(otp_secret_key, interval=60)
        			print(user_email)
        			if totp.verify(otp) and user_email is not None:
        				user = User.objects.get(email=user_email)
        				if user is not None:
	        				login(request,user, backend = 'django.contrib.auth.backends.ModelBackend')
	        				del request.session['otp_secret_key']
	        				del request.session['otp_valid_date']
	        				del request.session['user_email']
	        				user = request.user
	        				user_name = user.name.split(" ")
	        				name = user_name[0]

	        				id_user = urlsafe_base64_encode(json.dumps(user.email+user.name).encode('utf-8'))
	        				redirect_url = reverse('user_manager:profil_home', kwargs={'user_name':name,'user_token':id_user})

	        				# redirect_url = reverse("user_manager:profil_home",kwargs={'user_name':name,'user_token':user.token})

	        				return redirect(redirect_url)
	        				# return HttpResponse("is connected", content_type="text/plain")

        			else:
        				messages.info(request,"Invalid authentication code")
        		else:
        			messages.info(request,"Vore code d'authentication a expire")
        	else:
        		pass
        else:
        	context['errors'] = form.errors.items()
        return render(request, self.template_name, context)

 	# ask new code otp
    @classmethod
    def generate_new_otp_secret_code(cls,request):
    	if 'user_email' in request.session:
	    	user_email = request.session['user_email']
	    	user = User.objects.get(email=user_email)
	    	if user is not None:
	    		instance = cls()
	    		try:
	    			instance.send_otp(request,user,user.email)
	    		except Exception as e:
	    			messages.info(request,"!!! Des soucis lors de l'envoi du code, reessayez plus tard")
	    		else:
	    			return redirect("auth:2fa_account_verify")
	    	else:
	    		messages.warning(request,"Aucun utilisateur trouve.")
    	else:
	    	return redirect("auth:sign_in")

###########################################################
##################### FORGOT PASSWORD #####################
##########################################################
@method_decorator(guest_only,name='dispatch')
class UserForgotPassword(ActivationAccount,FormView):
	template_name = 'authentication/forgotPassword/get_forgot_password_email.html'
	page_title='Forgot password'
	mail_subject="Reinitialisation de mot de passe"
	template_mail="authentication/forgotPassword/template_forgot_password.html"
	template_name = 'authentication/pages/get_forgot_password_email.html'
	page_title='Forgot password'

	def get(self,request):
		form = UserForgotPasswordForm()
		context={'form':form,'page_title':self.page_title}
		return render(request,self.template_name,context)

	def post(self,request):
		form = UserForgotPasswordForm(request.POST, error_class=ParagraphErrorList)
		context={'form':form,'page_title':self.page_title}
		mail_subject="Change password"
		template_mail="authentication/forgotPassword/template_forgot_password.html"
		if form.is_valid():
			user=User.objects.filter(email=form.cleaned_data.get('email')).first()
			try:
				self.send_user_mail(request,user,user.email,mail_subject,template_mail)
			except Exception as e:
				messages.info(request,'Echec de l\'envoi de mail verifiez la connexion ')
			else:
				return self.forgot_password_mail_send_success(request)
		if form.is_valid():
			email = form.cleaned_data.get('email')
			user=User.objects.filter(email=email).first()
			self.send_forgot_password_mail(request,user,email)
			return self.forgot_password_mail_send_success(request)
		if not form.is_valid():
			context['errors'] = form.errors.items()

		return render(request,self.template_name,context)

	@classmethod
	def forgot_password_mail_send_success(cls,request):
		return render(request, 'authentication/forgotPassword/forgot_password_mail_send_success.html',{'page_title':'succès'})
		return render(request, 'authentication/forgot_password_mail_send_success.html')

	@classmethod
	def reset_password(cls, request, uidb64, token):
	    try:
	        uid = force_str(urlsafe_base64_decode(uidb64))
	        user = User.objects.get(pk=uid)
	    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
	        user = None

	    if user is not None:
	        if request.method == 'POST':
	            form = ResetPasswordForm(request.POST, error_class=ParagraphErrorList)
	            if form.is_valid():
	            	new_password = form.cleaned_data.get('password')
	            	user.password=make_password(new_password)
	            	user.save()
	            	return redirect('auth:sign_in')
	            else:
	                context = {'form': form, 'errors': form.errors.items()}
	        else:
	            form = ResetPasswordForm()
	        context = {'form': form,'uid':uidb64,'token':token,'page_title':'Reset password'}

	        return render(request, 'authentication/forgotPassword/reset_password.html', context)
	    else:
	        # Handle the case when user is None
	        pass


