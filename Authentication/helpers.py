from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.contrib import messages
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_str
from django.core.mail import EmailMessage,BadHeaderError,send_mail

import pyotp
from datetime import datetime, timedelta

# owns imports
from .token import account_activate_token
User=get_user_model()

class ActivationAccount():
	# send email activating link
	def send_user_mail(self,request, user, to_email, mail_subject, template_mail, otp_code=" "):
	    message = render_to_string(template_mail,{
	        'user':user,
	        'otp_code' : otp_code,
	        'domain':get_current_site(request).domain,
	        'uid':urlsafe_base64_encode(force_bytes(user.pk)),
	        'token':account_activate_token.make_token(user),
	        'protocol':'https' if request.is_secure() else 'http'
	    })
	    email = EmailMessage(mail_subject, message, to=[to_email])
	    email.content_subtype = 'html'
	    email.send()

	# send opt secret key
	def send_otp(self,request,user,user_mail):
		# utilitis
		template_mail = "authentication/activation/template_2fa_verify.html"
		mail_subject = "Verification de votre identite"
		# otp logic traitemnt
		totp = pyotp.TOTP(pyotp.random_base32(), interval=60)
		otp = totp.now()
		request.session['otp_secret_key'] = totp.secret
		valid_date = datetime.now()+timedelta(hours=24)
		request.session['otp_valid_date'] = str(valid_date)
		# send mail
		self.send_user_mail(request,user,user_mail,mail_subject,template_mail,otp)


	  

	

