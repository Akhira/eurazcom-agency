from django.urls import path

app_name = 'auth'

from .views import(
	UserCreationAccountView,
	UserConnexionAccountView,
	User2FAView,
	UserForgotPassword,


	# def function logout
	# account_logout
)

urlpatterns = [
	path('signup/',UserCreationAccountView.as_view(),name='sign_up'),
	path('activate/<uidb64>/<token>/', UserCreationAccountView.activate_account, name='activate_account'),
	# path('account-create-successfuly/',UserCreationAccountView.account_create_success, name='account_create_and_sendmail_successfuly'),
	path('new_activation_link/',UserCreationAccountView.generate_new_activation_link, name='resent_new_activation_link'),
	# path('account-create-failed/',UserCreationAccountView.account_create_failed, name='account_create_and_sendmail_failed'),
	# path('account/<uidb64>/<token>/activate-success/',UserCreationAccountView.account_activate_success, name='account_activate_success'),
	# path('account/<uidb64>/<token>/activate-failed/',UserCreationAccountView.account_activate_failed, name='account_activate_failed'),
	# path('account/<uidb64>/<token>/already-activate/', UserCreationAccountView.account_already_activate, name='account_already_activate'),

	# login system
	path('signin/',UserConnexionAccountView.as_view(),name='sign_in'),
	path('account/suspended/',UserConnexionAccountView.account_suspended,name='account_suspended'),	
	path('account/not-actived/',UserConnexionAccountView.account_not_actived,name='account_suspended'),	
		
	path('account_verify/',User2FAView.as_view(),name='2fa_account_verify'),	
	path('new_account_verify_otp_code/',User2FAView.generate_new_otp_secret_code,name='new_otp_secret_code'),	

	# Forgot password
	path('account/forgot-password',UserForgotPassword.as_view(),name='forgot_password'),
	path('account/reset-password-success',UserForgotPassword.forgot_password_mail_send_success,name='reset_password_success'),
	path('account/reset-password/<uidb64>/<token>/',UserForgotPassword.reset_password,name='reset_password')

]