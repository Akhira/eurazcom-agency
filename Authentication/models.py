from django.db import models
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin
from .manager import CustomUserManager


class CustomUser(AbstractBaseUser):
	name = models.CharField(max_length=150)
	email = models.EmailField(max_length=150,unique=True)
	phone = models.CharField(max_length=150,unique=True)
	country = models.CharField(max_length=50,default='CR')
	city = models.CharField(max_length=50,default='CR')
	state = models.CharField(max_length=50,default='CR')
	status = models.CharField(max_length=50,default='CR')

	activity_sector = models.CharField(max_length=150)
	avatar = models.ImageField(max_length=30,null=True,blank=True)

	is_active = models.BooleanField(default=False)
	is_suspended = models.BooleanField(default=False)
	is_closed = models.BooleanField(default=False)
	is_staff = models.BooleanField(default=False)
	is_superuser = models.BooleanField(default=False)


	USERNAME_FIELD  = 'email'

	REQUIRED_FIELDS=['name','phone']

	objects = CustomUserManager()

	# superuser = objects.create_superuser(email='superuser@example.com', name='Serge',phone='+237680638086', password='password')



	def __str__(self):
	    return self.name
    
	# def update_fields(self,**kwargs):
	# 	fields_to_update = kwargs.keys()
	# 	valid_fields = set(fields_to_update) & set(self._meta.get_fields())
	# 	if valid_fields:
	# 		for field_name in valid_fields:
	# 			setattr(self,field_name,kwargs[field_name])
	# 			self.save()