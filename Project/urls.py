from django.urls import path

app_name = 'project'

from .views import(
	CreateProject,
	CreateProjectCategory,
	CreateProjectDetails,
	ProjectBudgetAndScheduler,
	ProjectGraphicDesign,
	ProjectIngineering,
	ProjectAdvertising,
	ShowDetailsProjectByCode,

	# utilities
	ProjectUtilities,
)

urlpatterns = [
	path('u=<str:user_name>?id=<str:user_token>/project_start',CreateProject.as_view(),name='create_project_start'),
	path('u=<str:user_name>?id=<str:user_token>/?project.id=_<str:project_number>/category',CreateProjectCategory.as_view(),name='project_category'),
	path('u=<str:user_name>?id=<str:user_token>/?project.id=_<str:project_number>/details',CreateProjectDetails.as_view(),name='project_details'),
	path('u=<str:user_name>?id=<str:user_token>/?project.id=_<str:project_number>/ingineering',ProjectIngineering.as_view(),name='project_ing'),
	path('u=<str:user_name>?id=<str:user_token>/?project.id=_<str:project_number>/design_graphic',ProjectGraphicDesign.as_view(),name='required_mkt'),
	path('u=<str:user_name>?id=<str:user_token>/?project.id=_<str:project_number>/advertising',ProjectAdvertising.as_view(),name='required_ads'),
	path('u=<str:user_name>?id=<str:user_token>/?project.id=_<str:project_number>/budget_schedule',ProjectBudgetAndScheduler.as_view(),name='project_budget_schedule'),
	path('u=<str:user_name>?id=<str:user_token>/?project.id=_<str:project_number>/project_show',ShowDetailsProjectByCode.as_view(),name='project_show'),
	
	path('u=<str:user_name>?id=<str:user_token>/?project.id=_<str:project_number>/fonctionalities',ProjectFonctionalities.as_view(),name='project_fonctionalities'),
 

	# utilities
	path('project/<int:project_number>/submit_project/',ProjectUtilities.submit_project,name='project_submit'),
	path('project/<int:project_number>/delete/',ProjectUtilities.destroy_project,name='project_destroy'),
	path('project/<int:project_number>/follow/',ProjectUtilities.follow_project,name='project_follow'),
	path('project/<int:project_number>/recognize/',ProjectUtilities.recognize_project,name='project_recognize'),
	path('project/search/?q=>/',ProjectUtilities.search_project,name='project_search'),

# project_ing
# project_mkt
# project_ads
]