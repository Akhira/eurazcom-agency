from django.urls import reverse
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

from helpers import Utilities 

User = get_user_model()

class Project(models.Model,Utilities):
	user = models.ForeignKey(User, related_name='projects', on_delete=models.CASCADE, null=True,blank=True, default=None)
	number = models.IntegerField(unique=True)
	intern_ref = models.CharField(max_length=10, unique=True, null=True,blank=True)
	category = models.CharField(max_length=10, null=True,blank=True)
	is_submited = models.BooleanField(default=False)
	is_follow = models.BooleanField(default=False)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now_add=True)


	def follow_project_model(self):
		self.is_follow = not self.is_follow
		self.save()

	def search_project_model(self,query):
		try:
			projects = self.ProjectDetails
			return projects.search_project_by_name(query)
		except ProjectDetails.DoesNotExists:
			return []

	def  get_absolute_url(self):
		redirect_url = self.redirect_with_user_kwargs("project:project_category",self.number)
		return reverse(redirect_url)
			


class ProjectDetails(models.Model):
	project = models.OneToOneField(Project, related_name='project', on_delete=models.CASCADE)
	title = models.CharField(max_length=100)
	slogan = models.CharField(max_length=100)
	activity = models.CharField(max_length=100)
	colors = models.CharField(max_length=100)
	objectifs = models.TextField(max_length=5000)

	# empty fields
	presentation = models.TextField(max_length=15000,null=True,blank=True)
	start_date = models.DateField(auto_now_add=False,null=True,blank=True)
	end_date = models.DateField(auto_now_add=False,null=True,blank=True)
	duration = models.CharField(max_length=10,null=True,blank=True)
	budget = models.FloatField(null=True,blank=True)
	curency = models.CharField(max_length=3, null=True,blank=True)

	def search_project_by_name(self,query):
		return self.__class__.objects.filter(Q(project__code__icontains=query) | Q(title__icontains=query))


class  Ingineering(models.Model):
	project = models.OneToOneField(Project, related_name='projects', on_delete=models.CASCADE)



class  GraphicDesign(models.Model):
	project = models.OneToOneField(Project, related_name='project_services', on_delete=models.CASCADE)


class  Advertising(models.Model):
	project = models.OneToOneField(Project, related_name='project_services', on_delete=models.CASCADE)