from django.shortcuts import render,redirect
from django.urls import reverse
from django.views.generic import FormView,TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.http import urlsafe_base64_encode
from helpers import Utilities

from .models import (
		Project,
		ProjectDetails,
		Ingineering,
		GraphicDesign,
		Advertising,
	)
from .forms import (
		CategoryFrom,
		ProjectPresentationForm,
		ProjectDatailsForm,
		ProjectSearchForm,
		ProjectIngineeringForm,
		ProjectGraphicDesignForm, 
		ProjectAdvertisingForm,
		ParagraphErrorList
	)


# 
class GetProjectByNumber():
	def get_project_by_number(self,request,project_number):
		try:
			project = Project.objects.filter(user=request.user,number=project_number)
		except Exception as e:
			project = None
			return render(request,'project/exceptions/project_not_found.html')
		else:
			return project

	def get_project_details_by_project(self,request,project_number):
		project = self.get_project_by_number(request,project_number)
		try:
			project_detail = ProjectDetails.objects.get(project=project)
		except ProjectDetails.DoesNotExists:
			project_detail=None
			return render(request,'project/exceptions/project_details_not_found.html')
		else:
			return project,project_detail

	def delete_model_if_category_change(self,models,project_number):

		for model in models:
			model.objects.filter(project__number = project_number).delete()
	
 

# Start and create project
class CreateProject(Utilities, TemplateView):
	template_name = "project/start_create_project_view.html"
	page_title = "start create project"

	def get(self,request,user_name,user_token):
		context = {'page_title':self.page_title}
		return render(request,self.template_name,context)

	def post(self,request):
		template_mail="project/template_project_mail_view.html"
		mail_subject = "Creation de project"
		user = request.user
		project = Project.objects.create(
				number = self.generate_project_number(),
				user = user,
			)
		project.save()
		# send mail wih project code
		self.send_user_mail(request,user,user.email,mail_subject,template_mail,project.number)
		route="project:project_category"
		redirect_url = self.redirect_with_user_kwargs(route,project.number)
		return redirect(redirect_url)


# project category
class CreateProjectCategory(GetProjectByNumber, Utilities, FormView):
	template_name = "project/category_create_project_view.html"
	page_title = "start create project"

	def get(self,request,user_name,user_token, project_number):
		project = self.get_project_by_number(request,project_number)
		form = CategoryFrom(instance=project)
		context = {'page_title':self.page_title,'form':form}
		return render(request,self.template_name,context)

	def post(self,request,user_name,user_token,project_number):
		project = self.get_project_by_number(request,project_number)
		form = CategoryFrom(instance=project, error_class=ParagraphErrorList)
		context = {'page_title':self.page_title,'form':form}
		if form.is_valid():
			project_form = form.save(commit=False)
			project_form.intern_ref = self.generate_project_ref()
			category_input = form.cleaned_data.get('category')
			category_list = ("ING",'GRD','ADS')
			
			if project.category != category_input:
				if category_input == 'ING':
					self.delete_model_if_category_change([GraphicDesign,Advertising],project_number)
				if category_input == 'GRD':
					self.delete_model_if_category_change([Ingineering,Advertising],project_number)
				if category_input == 'ADS':
					self.delete_model_if_category_change([GraphicDesign,Ingineering],project_number)
			project_form.category = category_input
			project_form.save()
			redirect_url = self.redirect_with_user_kwargs("project:project_details",project.number)
			return redirect(redirect_url)

		if not form.is_valid():
			context['errors'] = form.errors.items()
		return render(request,self.template_name,context)

# project details
class CreateProjectDetails(GetProjectByNumber, Utilities, FormView):
	template_name = "project/details_create_project_view.html"
	page_title = "Project informations"

	
	def get(self,request,user_name,user_token, project_number):
		project,project_detail = self.get_project_details_by_project(request,project_number)
		form = ProjectDetailsFrom(instance=project_detail)
		context = {
				'page_title':self.page_title,
				'form':form
			}
		return render(request,self.template_name,context)

	def post(self,request,user_name,user_token,project_number):
		project,project_detail = self.get_project_details_by_project(request,project_number)
		form = ProjectDetailsFrom(instance=project_detail, error_class=ParagraphErrorList)
		context = {'page_title':self.page_title,'form':form}
		if form.is_valid():
			project_detail_form = form.save(commit=False)
			project_detail_form.project = project
			project_detail_form.save()
			route="project:project_details"
			redirect_url = self.redirect_with_user_kwargs(route,project.number)
			return redirect(redirect_url)

		if not form.is_valid():
			context['errors'] = form.errors.items()
		return render(request,self.template_name,context)

# project description/presentation
class ProjectDescription(GetProjectByNumber, Utilities, FormView):
	template_name = "project/description_project_view.html"
	page_title = "Presentation"

	def get(self,request,user_name,user_token, project_number):
		project,project_detail = self.get_project_details_by_project(request,project_number)
		form = ProjectPresentationForm(instance=project_detail)
		context = {'page_title':self.page_title,'form':form}
		return render(request,self.template_name,context)

	def post(self,request,user_name,user_token,project_number):
		project,project_detail = self.get_project_details_by_project(request,project_number)
		form = ProjectPresentationForm(instance=project_detail, error_class=ParagraphErrorList)
		context = {'page_title':self.page_title,'form':form}
		if form.is_valid():
			form.save(commit=True)
			categories_tulpe = ('ING','GRD','ADS')
			route = None
			for element in categories_tulpe:
				if project.category == element:
					route="project:project_"+element.lower()
			redirect_url = self.redirect_with_user_kwargs(route,project.number)
			return redirect(redirect_url)

		if not form.is_valid():
			context['errors'] = form.errors.items()
		return render(request,self.template_name,context)



# Technical needs
class ProjectIngineering(GetProjectByNumber, Utilities, FormView):
	template_name = "project/ingineering_view.html"
	page_title = "Technical project needs"
	def get(self,request,project_number):
		project = self.get_project_by_number(request,project_number)
		form = ProjectIngineeringForm(instance=project)
		context={'form':form,'page_title':self.page_title}
		return render(request,self.template_name,context)

	def post(self,request,project_number):
		project = self.get_project_by_number(request,project_number)
		form = ProjectIngineeringForm(request.POST, request.FILE,instance=project, error_class=ParagraphErrorList)
		context = {'form':form, 'project_title':self.project_title}
		if form.is_valid():
			technical_needs_form = form.save(commit=False)
			technical_needs_form.project = project
			technical_needs_form.save()

			redirect_url = self.redirect_with_user_kwargs("project:project_fonctionalities",project.number)
			return redirect(redirect_url)
		else:
			context['errors'] = form.errors.items()
		return render(request,self.template_name,context)

# Graphics project
class ProjectGraphicDesign(GetProjectByNumber, Utilities, FormView):
	template_name = "project/project_graphic_view.html"
	page_title = "Project required services"
	def get(self,request,project_number):
		project = self.get_project_by_number(request,project_number)
		form = ProjectGraphicDesignForm(instance=project)
		context={'form':form,'page_title':self.page_title}
		return render(request,self.template_name,context)

	def post(self,request,project_number):
		project = self.get_project_by_number(request,project_number)
		form = ProjectGraphicDesignForm(request.POST, request.FILE, instance=project,  error_class=ParagraphErrorList)
		context = {'form':form, 'project_title':self.project_title}
		if form.is_valid():
			required_service = form.save(commit=False)
			required_service.project = project
			required_service.save()

			redirect_url = self.redirect_with_user_kwargs("project:project_budget_schedule",project.number)
			return redirect(redirect_url)
		else:
			context['errors'] = form.errors.items()
		return render(request,self.template_name,context)

# Advitising project
class ProjectAdvertising(GetProjectByNumber, Utilities, FormView):
	template_name = "project/project_advitising_view.html"
	page_title = "Project required services"
	def get(self,request,project_number):
		project = self.get_project_by_number(request,project_number)
		form = ProjectAdvertisingForm(instance=project)
		context={'form':form,'page_title':self.page_title}
		return render(request,self.template_name,context)

	def post(self,request,project_number):
		project = self.get_project_by_number(request,project_number)
		form = ProjectAdvertisingForm(request.POST, request.FILE, instance=project,  error_class=ParagraphErrorList)
		context = {'form':form, 'project_title':self.project_title}
		if form.is_valid():
			required_service = form.save(commit=False)
			required_service.project = project
			required_service.save()

			redirect_url = self.redirect_with_user_kwargs("project:project_budget_schedule",project.number)
			return redirect(redirect_url)
		else:
			context['errors'] = form.errors.items()
		return render(request,self.template_name,context)

# Budget and Schedule
class ProjectBudgetAndScheduler(GetProjectByNumber, Utilities, FormView):
	template_name = "project/budget_schedule_project_details.html"
	page_title = "Budget,Schedule"

	def get(self,request,user_name,user_token, project_number):
		project,project_detail = self.get_project_details_by_project(request,project_number)
		form = ProjectPresentationForm(instance=project_detail)
		context = {'page_title':self.page_title,'form':form}
		return render(request,self.template_name,context)

	def post(self,request,user_name,user_token,project_number):
		project,project_detail = self.get_project_details_by_project(request,project_number)
		form = ProjectPresentationForm(instance=project_detail, error_class=ParagraphErrorList)
		context = {'page_title':self.page_title,'form':form}
		if form.is_valid():
			form.save(commit=True)
			categories_tulpe = ('ING','MKT','ADS')
			route = "project:project_conditions"
			redirect_url = self.redirect_with_user_kwargs(route,project.number)
			return redirect(redirect_url)

		if not form.is_valid():
			context['errors'] = form.errors.items()
		return render(request,self.template_name,context)

# Ingineering project fonctionalities 
class ProjectFonctionalities(GetProjectByNumber, Utilities, FormView):
	template_name = "project/project_ingineering_fties_view.html"
	page_title = "Budget,Schedule"

	def get(self,request,user_name,user_token, project_number):
		project,project_detail = self.get_project_details_by_project(request,project_number)
		form = ProjectPresentationForm(instance=project_detail)
		context = {'page_title':self.page_title,'form':form}
		return render(request,self.template_name,context)

	def post(self,request,user_name,user_token,project_number):
		project,project_detail = self.get_project_details_by_project(request,project_number)
		form = ProjectPresentationForm(instance=project_detail, error_class=ParagraphErrorList)
		context = {'page_title':self.page_title,'form':form}
		if form.is_valid():
			form.save(commit=True)
			categories_tulpe = ('ING','MKT','ADS')
			route = "project:project_conditions"
			redirect_url = self.redirect_with_user_kwargs(route,project.number)
			return redirect(redirect_url)

		if not form.is_valid():
			context['errors'] = form.errors.items()
		return render(request,self.template_name,context)

# show project
class ShowDetailsProjectByCode(LoginRequiredMixin,GetProjectByNumber,Utilities,TemplateView):
	template_name = "project/get_single_project.html"
	def get(self,request,project_number):
		project,project_detail = self.get_project_details_by_project(request,project_number)
		context = {'project':project,'page_title':project.project.title}
		return render(request,self.template_name,context)

	def post(self,request,project_number):
		pass

#Utilities
class ProjectUtilities(LoginRequiredMixin,GetProjectByNumber, Utilities,TemplateView):
	message_str = "Vous ne possedez pas droits pour modifier ou agir sur ce projet"
	 #submit project for realization
	@classmethod
	def submit_project(cls,request,project_number):
		instance = cls()
		project = instance.get_project_by_number(request,project_number)
		if project.user is not None and project.user == request.user:
			# verify if project is correct
			models_tulpes = (Project,ProjectDetails)
			for model_elt in models_tulpes:
				model_with_field = self.check_required_fields(model_elt)
				if model_with_field is not None:
					route_model_view = model_with_field.get_absolute_url()
					redirect_url = self.redirect_with_user_kwargs(route_model_view,project.number)
					return redirect(redirect_url)
				else:
					project.is_submited = True
					project.save()
					messages.success(request,"succes soumission")
					return redirect(self.redirect_with_user_kwargs("project:project_show_view",project.number)) 
		else:
			messages.error(request,cls.message_str)
		view_name_route = request.resolver_match.view_name
		redirect_url = instance.redirect_with_user_kwargs(view_name_route,project.number)
		return redirect(redirect_url)

	# destroy project
	@classmethod
	def destroy_project(cls,request,project_number):
		instance = cls()
		project=instance.get_project_by_number(request,project_number)
		if project.user is not None and project.user == request.user:
			project.delete()
			redirect_url = self.redirect_with_user_kwargs("user_manager:profil_home")
			return redirect(redirect_url)
		else:
			messages.error(request, cls.message_str)
			return redirect(self.redirect_with_user_kwargs("project:project_show_view",project.number)) 


	# follow project
	@classmethod
	def follow_project(cls,request,project_number):
		instance = cls()
		view_name_route = request.resolver_match.view_name
		project=instance.get_project_by_number(request,project_number)
		if project.user is not None and project.user == request.user:
			project.follow_project_model()
			# redirect_url = self.redirect_with_user_kwargs(view_name_route)
			# return redirect(redirect_url)
		else:
			messages.error(request, cls.message_str)
			# return redirect(self.redirect_with_user_kwargs(view_name_route,project.number))
	
	# Recognize project
	@classmethod
	def recognize_project(cls,request,project_number):
		project = self.get_project_by_number(request,project_number)
		user = request.user
		redirect_url = self.redirect_with_user_kwargs("project:project_show",project.number)
		if user == project.user:
			project.user = user
			project.save()
		else:
			messages.error(request,"Vous n'etes pas autorise a effectuer des actions sur ce projet")
		return redirect(redirect_url)


	# search project
	@classmethod
	def search_project(cls,request):
		form = ProjectSearchForm(request.POST)
		projects = []
		query = request.POST('query_input')
		projects_finds = Project.search_project_model(query)
		if projects_finds.count() > 0:
			projects = projects_finds
		context={'projects':projects}
		return render(request,"project/projects_search_results.html",context)
	
	