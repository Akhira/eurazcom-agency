from django import forms
from django.forms.utils import ErrorList
from .models import ProjectDetails

class ParagraphErrorList(ErrorList):
    def __str__(self):
        return self.as_divs()

    def as_divs(self):
        if not self:return ''
        return '<div class="errorlist text-danger">%s</div>'%''.join(['<p class="small error">%s</p>'%e for e in self])

class CategoryFrom(forms.Form):
	CHOICE_CAT = (
		('ING','Ingenierie Logicielles'),
		('MKT','Ingenierie Visuelle'),
		('ADS','Publicite'),
	)
	category = forms.ChoiceField(
		label='A quelle categorie appartient votre projet ?',
		choices=CHOICE_CAT, 
		widget=forms.RadioSelect(attrs={'class':'form-check-input'}))

class ProjectDatailsForm(forms.ModelForm):
	class Meta:
		model=ProjectDetails
		fields = ['title','slogan','activity','colors','objectifs']

	def __init__(self,*args, **kwargs):
		super(ProjectDatailsForm,self).__init__(*args, **kwargs)
		for field_name, field in self.fields.items():
			 widgets_class = field.widget.attrs.get('class','').split()
			 if 'form-check-input' not in widgets_class:
					field.widget.attrs['class'] = field.widget.attrs.get('class', '') + ' form-control'
					field.widget.attrs['style'] = field.widget.attrs.get('style', '') + 'box-shadow:none;'


class ProjectPresentationForm(forms.ModelForm):
	class Meta:
		model=ProjectDetails
		fields=['presentation']

	def __init__(self, *arg,**kwargs):
		super(ProjectPresentationForm, self).__init__(*arg,**kwargs)
		for field_name,field in self.fields.items():
			field.widget.attrs['class'] = field.widget.attrs.get('class', '') + ' form-control'
			field.widget.attrs['style'] = field.widget.attrs.get('style', '') + 'box-shadow:none;'


class ProjectBudgetAndSchedulerForm(forms.ModelForm):
	class Meta:
		model=ProjectDetails
		fields = ['start_date','end_date','duration','budget','curency']

	def __init__(self,*args, **kwargs):
		super(ProjectBudgetAndSchedulerForm,self).__init__(*args, **kwargs)
		for field_name, field in self.fields.items():
			 widgets_class = field.widget.attrs.get('class','').split()
			 if 'form-check-input' not in widgets_class:
					field.widget.attrs['class'] = field.widget.attrs.get('class', '') + ' form-control'
					field.widget.attrs['style'] = field.widget.attrs.get('style', '') + 'box-shadow:none;'


class ProjectIngineeringForm(forms.ModelForm):
	pass

class ProjectGraphicDesignForm(forms.ModelForm):
	pass

class ProjectAdvertisingForm(object):
	 pass
# Search projects form
class ProjectSearchForm(forms.Form):
	query_input = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter code or title of project'}))