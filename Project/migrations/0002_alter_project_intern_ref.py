# Generated by Django 3.2.24 on 2024-03-28 16:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Project', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='intern_ref',
            field=models.CharField(blank=True, max_length=10, null=True, unique=True),
        ),
    ]
