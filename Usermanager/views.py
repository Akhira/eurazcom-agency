from django.contrib.auth.decorators import login_required
from django.shortcuts import render,redirect
from django.contrib.auth import logout
from django.contrib.auth.hashers import make_password
from django.contrib import messages
from .models import Security
from  .forms import UserForm,UserAvatarForm,UserChangePasswordForm,ParagraphErrorList



# Get user profil home
@login_required
def get_user_profil_home(request, user_name, user_token):
	return render(request,"usermanager/user_profil_home.html")

###############################################
############### DATAS MANAGEMENT ##############
###############################################
@login_required
def get_user_data_page(request):
	return render(request,"usermanager/user_data.html",context)

@login_required
def update_user_datas(request):
	if request.method == 'POST':
		user_update_form = UserForm(request.POST, instance=request.user, error_class=ParagraphErrorList)
		if user_update_form.is_valid():
			user_update_form.save()
			return redirect('user_manager:get_datas_page')
	else:
		user_update_form = UserForm(instance=request.user)
	context={'form':user_update_form,'page_title':request.user.name}
	return render(request,"usermanager/user_data.html",context)


###############################################
############## AVATAR MANAGEMENT ##############
###############################################
@login_required
def get_user_avatar_page(request):
	context = {'user':request.user}
	return render(request,"usermanager/user_data.html",context)

@login_required
def update_user_avatar(request):
	if request.method == 'POST':
		user_avatar_form = UserAvatarForm(request.POST, request.FILES, instance=request.user, error_class=ParagraphErrorList)
		if user_avatar_form.is_valid():
			user_avatar_form.save()
			return redirect('user_manager:get_avatar_page')
	else:
		user_avatar_form = UserAvatarForm(instance=request.user)
	context={'form':user_avatar_form,'page_title':request.user.name}
	return render(request,"usermanager/user_avatar.html",context)

###############################################
############ PASSWORD MANAGEMENT ##############
###############################################
@login_required
def update_user_password(request):
	user=request.user
	if request.method == 'POST':
		password_form = UserChangePasswordForm(request.POST, instance=user, error_class=ParagraphErrorList)
		if user_avatar_form.is_valid():
			user_form = password_form.save(commit=False)
			user_form.password = make_password(form.cleaned_data.get('password'))
			user_form.save()
			return redirect('user_manager:get_avatar_page')
	else:
		password_form = UserChangePasswordForm(instance=request.user)
	context={'form':password_form,'page_title':request.user.name}
	return render(request,"usermanager/user_password_change.html",context)
 


###############################################
################ DELETE ACCOUNT ###############
###############################################
@login_required
def delete_account(request):
	user = request.user
	user.is_closed = True
	user.save()
	logout(request, user)
	messages.success(request,"Compte supprimer avec succes")
	return redirect("auh:sign_in")


#logout
@login_required
def account_logout(request):
	logout(request)
	return redirect('auth:sign_in')



# def update_user_profil(request):
# 	user = request.user
# 	if request.method == 'POST':
# 		user_update_form = UserForm(request.POST, instance=user, error_class=ParagraphErrorList)
# 		if user_update_form.is_valid():
# 			updates_fields = user_update_form.cleaned_data
# 			CustomUser.update_fields(**updates_fields)
# 			return redirect("")
# 	else:
# 		user_update_form = UserForm(instance=user)
# 		return render(request,"usermanager/user_data.html",context)






