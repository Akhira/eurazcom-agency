from django.db import models
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

User=get_user_model()

class Security(models.Model):
	user = models.OneToOneField(User, on_delete = models.CASCADE)

	two_factor = models.BooleanField(default=False)
	c_password = models.BooleanField(default=False)
	close_account = models.BooleanField(default=False)


 