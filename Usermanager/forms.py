from django import forms
import re
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.forms.utils import ErrorList

# import owns models
User=get_user_model()

class ParagraphErrorList(ErrorList):
    def __str__(self):
        return self.as_divs()

    def as_divs(self):
        if not self:return ''
        return '<div class="errorlist text-danger">%s</div>'%''.join(['<p class="small error">%s</p>'%e for e in self])


class UserForm(forms.ModelForm):
	class Meta:
		model = User
		fields= ['name','email','phone','status','country','city','state','status','activity_sector']

	def __init__(self, *args, **kwargs):
	    super(UserForm, self).__init__(*args, **kwargs)
	    for field_name, field in self.fields.items():
	        widget_class = field.widget.attrs.get('class', '').split()
	        if 'form-check-input' not in widget_class:      					
	            field.widget.attrs['class'] = field.widget.attrs.get('class', '') + ' form-control'
	            field.widget.attrs['style'] = field.widget.attrs.get('style', '') + 'box-shadow:none;'
	            
 
class UserAvatarForm(forms.ModelForm):
	class Meta:
		model = User
		fields= ['avatar']

	def __init__(self, *args, **kwargs):
	    super(UserForm, self).__init__(*args, **kwargs)
	    for field_name, field in self.fields.items():     					
	        field.widget.attrs['class'] = field.widget.attrs.get('class', '') + ' form-control'
	        field.widget.attrs['style'] = field.widget.attrs.get('style', '') + 'box-shadow:none;'
	            
class UserChangePasswordForm(forms.Form):
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'votre mot de passe'}))
	password_confirm = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Confirmez votre mot de passe'}))

	def clean_password(self):
		password = self.cleaned_data.get('password')
		if len(password) < 8:
			raise forms.ValidationError("Le mot de passe doit comporter au moins 8 caractères.")

		if not re.search(r'[A-Z]', password):
			raise forms.ValidationError("Le mot de passe doit contenir au moins une lettre majuscule.")

		if not re.search(r'\d', password):
			raise forms.ValidationError("Le mot de passe doit contenir au moins un chiffre.")

		if not re.search(r'[!@#$%^&*(),.?":{}|<>]', password):
			raise forms.ValidationError("Le mot de passe doit contenir au moins un caractère spécial.")

		return password

	def clean_password_confirm(self):
		password = self.cleaned_data.get('password')
		password_confirm = self.cleaned_data.get('password_confirm')

		if password and password_confirm and password != password_confirm:
			raise forms.ValidationError("Les mots de passe ne correspondent pas.")
		return password_confirm