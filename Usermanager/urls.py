from django.urls import path

app_name = 'user_manager'

from .views import(
	 get_user_data_page,
	 update_user_datas,
	 get_user_avatar_page,
	 update_user_avatar,
	 update_user_password,
	 get_user_profil_home,
	 delete_account,
	 account_logout,
)

urlpatterns = [
	
	path('profil/u=<str:user_name>?id=<str:user_token>/',get_user_profil_home, name='profil_home'),
	# datas routes
	path('infos_datas/',get_user_data_page,name='get_datas_page'),
	path('infos_datas/change/',update_user_datas,name='user_datas_updates'),

	# avatar route
	path('avatar_u/',get_user_avatar_page,name='get_avatar_page'),
	path('infos_datas/change/',update_user_avatar,name='user_avatar_updates'),

	# password & close account
	path('password_change/',update_user_password,name='user_password_change'),
	path('delete_account/',delete_account,name='delete_account'),
	path('logout/',account_logout,name='logout'),
	 
]